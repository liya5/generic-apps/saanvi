#!/bin/bash

######################################################################################################################

sudo pacman -S wget --noconfirm --needed

echo "Getting the Chaotic keys from the Hyderabad Mirror - report if link is broken"
sudo wget https://hyd1-in-mirror.silky.network/chaotic-aur/chaotic-aur/x86_64/chaotic-keyring-20230616-1-any.pkg.tar.zst -O /tmp/chaotic-keyring-20230616-1-any.pkg.tar.zst
sudo pacman -U --noconfirm --needed /tmp/chaotic-keyring-20230616-1-any.pkg.tar.zst

echo "Getting the latest arcolinux mirrors file - report if link is broken"
sudo wget https://hyd1-in-mirror.silky.network/chaotic-aur/chaotic-aur/x86_64/chaotic-mirrorlist-20230603-1-any.pkg.tar.zst -O /tmp/chaotic-mirrorlist-20230603-1-any.pkg.tar.zst
sudo pacman -U --noconfirm --needed /tmp/chaotic-mirrorlist-20230603-1-any.pkg.tar.zst
	
######################################################################################################################

if grep -q chaotic-aur /etc/pacman.conf; then

	echo "Chaotic-AUR Repo is already in /etc/pacman.conf"

else

echo '
[chaotic-aur]
SigLevel = Optional TrustedOnly
Include = /etc/pacman.d/chaotic-mirrorlist' | sudo tee --append /etc/pacman.conf

fi

echo "DONE - UPDATE NOW"
